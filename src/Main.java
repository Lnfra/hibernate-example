import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.persistence.metamodel.EntityType;

public class Main {
  private static final SessionFactory sessionFactory;

  static {
    try {
      Configuration configuration = new Configuration();
      configuration.configure();

      sessionFactory = configuration.buildSessionFactory();
    } catch (Throwable ex) {
      throw new ExceptionInInitializerError(ex);
    }
  }

  public static Session getSession() throws HibernateException {
    return sessionFactory.openSession();
  }

  public static void main(final String[] args) throws Exception {
    final Session session = getSession();
    try {
      System.out.println("querying all the managed entities...");
      final Metamodel metamodel = session.getSessionFactory().getMetamodel();
      for (EntityType<?> entityType : metamodel.getEntities()) {
        final String entityName = entityType.getName();
        final Query query = session.createQuery("from " + entityName);
        System.out.println("executing: " + query.getQueryString());
        for (Object o : query.list()) {
          System.out.println("  " + o.toString());
        }
      }
    } finally {
      session.close();
    }
  }
}