import com.tw.pathashala.hibernate.Book;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;

import java.util.List;

class MainTest {

  private static final SessionFactory sessionFactory;

  static {
    try {
      Configuration configuration = new Configuration();
      configuration.configure();
      sessionFactory = configuration.buildSessionFactory();
    } catch (Throwable ex) {
      throw new ExceptionInInitializerError(ex);
    }
  }

  public static Session getSession() throws HibernateException {
    return sessionFactory.openSession();
  }

  @Test
  void expectToCreateANewBookWithId12InDatabase() {
    Session session = getSession();
    session.beginTransaction();
    Book bookEntity = new Book();
    bookEntity.setId(12);
    bookEntity.setTitle("hello day2");
    session.save(bookEntity);
    session.getTransaction().commit();
    session.close();
  }

  @Test
  void expectToReadAllBooksFromDatabase() {
    Session session = getSession();
    session.beginTransaction();

    Query query = session.createQuery("from Book");
    List<Book> books = query.list();
    for (Book book : books) {
      System.out.println("Id: " + book.getId() + "    Title: " + book.getTitle());
    }
    session.getTransaction().commit();
    session.close();
  }

  @Test
  void expectToUpdateBookWithID10InDatabase() {
    Session session = getSession();
    session.beginTransaction();
    Book book = session.get(Book.class, 10);
    book.setTitle("This is an updated book");
    session.update(book);
    session.getTransaction().commit();
    session.close();
  }

  @Test
  void expectToDeleteBookWithID1FromDatabase() {
    Session session = getSession();
    session.beginTransaction();
    Book book = session.load(Book.class, 1);
    session.delete(book);
    session.getTransaction().commit();
    System.out.println("Deleted Successfully");
    session.close();
  }

  @Test
  void expectThatBookOfTitleTheHobbitIsRetrievedFromDatabase() {
    Session session = getSession();
    session.beginTransaction();

    String titleToFind = "The Hobbit";

    Query query = session.createQuery("from Book b where b.title like :title");
    query.setParameter("title", "%" + titleToFind + "%");

    List<Book> booksFound = query.list();
    for (Book book : booksFound) {
      System.out.println("Id: " + book.getId() + "    Title: " + book.getTitle());
    }

    session.getTransaction().commit();
    session.close();

  }
}